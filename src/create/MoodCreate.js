import React from 'react';
import { Create, SimpleForm, TextInput } from 'react-admin';

export default (props) => (
    <Create {...props} title="Création d'une Humeur">
        <SimpleForm>
            <TextInput label="Titre" source="label" />
            <TextInput label="Icône" source="icon" />
        </SimpleForm>
    </Create>
);
