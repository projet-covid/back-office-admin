import frenchTranslations from './french';
import polyglotI18nProvider from 'ra-i18n-polyglot';

const i18nProvider = polyglotI18nProvider( () =>
    frenchTranslations,
    'fr'
);

export default i18nProvider;
